<h1 align='center'> RN MOVIE <br>Movie Review</h1>

<br>

## Introduction

[![React Native](https://img.shields.io/badge/react%20native-v0.60.5-blue)](https://facebook.github.io/react-native/)

<p align='justify'>RN Movie is application to learn how to fetch open RESTAPI from the movie db, and also learn about Redux, Redux Persist, Redux-Saga
</p>

## Requirements

1. <a href="https://nodejs.org/en/download/">Node Js</a>
2. <a href="https://developer.android.com/studio">Android Studio<a/>
3. <a href="https://www.oracle.com/java/technologies/javase-jdk11-downloads.html">`Java SE Development Kit 11`<a/> or <a href="https://openjdk.java.net/install/">`openJDK 8`<a/>


## Installation

1. Download this Project or you can type `git clone https://github.com/audywp/front-end-PayLive.git`
2. Open app's directory in CMD or Terminal
3. Type `yarn install`
4. Type ` react-native run-android

Sometimes you may need to reset or clear the React Native packager's cache. To do so, you can pass the `--reset-cache` flag to the start script:

```
yarn start -- --reset-cache
```